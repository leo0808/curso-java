package pantallas.objetos.juegoFactory;

import pantallas.objetos.juegoStrategy.ComparacionStrategy;

public class Papel extends JuegoPiedraPapelTijera {
	//constructores
	public Papel(){
		this(JuegoPiedraPapelTijera.PAPEL, "PAPEL");
	}
	
	public Papel(int pValor, String pTextoValor) {
		super(pValor, pTextoValor);		
	}

	@Override
	public boolean isMe(int pConstJuego) {		
		return pConstJuego == JuegoPiedraPapelTijera.PAPEL;
	}

	@Override
	public int comparar(JuegoPiedraPapelTijera pJuegoppt) {
		setContrincante(pJuegoppt);
		return ComparacionStrategy.getInstance(this).getResultado();
	}

	@Override
	public String getResultado() {
		return ComparacionStrategy.getInstance(this).getTextoResultado();
	}

}
