package pantallas.objetos.juegoStrategy;

import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;

public class PiedraPierdeStrategy extends ComparacionStrategy {

	@Override
	public boolean isMe(JuegoPiedraPapelTijera pJuegoPpt) {
		return 	ComparacionStrategy.juegoPiedraPapelTijera.getValor() 					== juegoPiedraPapelTijera.PIEDRA &&
				ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getValor() == juegoPiedraPapelTijera.PAPEL;
	}

	@Override
	public int getResultado() {
		return juegoPiedraPapelTijera.PIERDE;
	}

	@Override
	public String getTextoResultado() {
		StringBuffer strTexto = new StringBuffer(ComparacionStrategy.juegoPiedraPapelTijera.getJugador().getAlias());
		strTexto.append(" con PIEDRA perdi� a ");
		strTexto.append(ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getJugador().getAlias());
		strTexto.append(" que eligi� PAPEL");
		return strTexto.toString();
	}

}
