package pantallas.objetos.juegoStrategy;

import java.util.ArrayList;
import java.util.List;

import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;

/**
 * @author Gabriel
 * Esta clase tiene la finalidad de establecer todas las estrategias relacionadas en el juego piedra, papel y tijera
 * esta clase conoce a todos sus hijos de devuelve la estrategia que corresponde 
 *
 */
public abstract class ComparacionStrategy {
	

	protected static JuegoPiedraPapelTijera juegoPiedraPapelTijera;
	
	/**
	 * Este metodo devuelve un valor num�rico que corresponde a las contantes definidas en JuegoPiedraPapelTijera.GANA, PIERDE 
	 * PIERDE o EMPATA que corresponde a 1; -1 y 0
	 * @return devuelve un valor num�rico que corresponde a JuegoPiedraPapelTijera.GANA, PIERDE 
	 * PIERDE o EMPATA que corresponde a 1; -1 y 0
	 */
	public abstract int getResultado();
	
	/**
	 * Este m�todo devuelve un texto indicando el que resultado del juego incluyendo indicando quien gano
	 * @return un String con el texto explicando el resultado.
	 */
	public abstract String getTextoResultado();
	
	/**
	 * Este m�todo tiene la finalidad de identificar si es la estrategia correcta comparando este objeto con el que se recibe 
	 * por par�metro
	 * @param pJuegoPpt es el par�metro que se recibe por par�metro ser comparado con el objeto actual
	 * @return devuelve verdadero para el caso que sea esta estrategia o falso en el caso contrario.
	 */
	public abstract boolean isMe(JuegoPiedraPapelTijera pJuegoPpt);
	
	/**
	 * Este m�todo devuelve el el obteto que cumple con la estrategia que se cumpla
	 * @param pJuego corresponde al objeto que se recibea para ser commparadado con este objeto y determinar la estrategia a seguir
	 * @return devuelve el objetot que corresponde a la estrategia
	 */
	public static ComparacionStrategy getInstance(JuegoPiedraPapelTijera pJuego) {
		juegoPiedraPapelTijera=pJuego;
		List<ComparacionStrategy> comparaciones= new ArrayList<ComparacionStrategy>();
		comparaciones.add(new PapelGanaStrategy());
		comparaciones.add(new PapelPierdeStrategy());
		comparaciones.add(new PiedraGanaStrategy());
		comparaciones.add(new PiedraPierdeStrategy());
		comparaciones.add(new TijeraGanaStrategy());
		comparaciones.add(new TijeraPierdeStrategy());
		comparaciones.add(new EmpateStrategy());
		for (ComparacionStrategy comparacionStrategy : comparaciones) {
			if(comparacionStrategy.isMe(pJuego))
				return comparacionStrategy;
		}
		
		return null;
	}


}
