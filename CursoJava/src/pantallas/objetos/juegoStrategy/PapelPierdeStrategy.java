package pantallas.objetos.juegoStrategy;

import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;

public class PapelPierdeStrategy extends ComparacionStrategy {

	@Override
	public int getResultado() {		
		return juegoPiedraPapelTijera.PIERDE;
	}

	@Override
	public String getTextoResultado() {
		StringBuffer strTexto = new StringBuffer(ComparacionStrategy.juegoPiedraPapelTijera.getJugador().getAlias());
		strTexto.append(" con PAPEL PIERDE contra ");
		strTexto.append(ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getJugador().getAlias());
		strTexto.append(" que eligi� TIJERA");
		
		return strTexto.toString();	
		}

	@Override
	public boolean isMe(JuegoPiedraPapelTijera pJuegoPpt) {
		return 	ComparacionStrategy.juegoPiedraPapelTijera.getValor() 					== juegoPiedraPapelTijera.PAPEL 	&&
				ComparacionStrategy.juegoPiedraPapelTijera.getContrincante().getValor() == JuegoPiedraPapelTijera.TIJERA;
	}

}
