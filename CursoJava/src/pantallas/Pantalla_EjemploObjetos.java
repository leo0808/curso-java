package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla_EjemploObjetos {

	private JFrame frame;
	private JTextField textPrimerValor;
	private JTextField textSegundoValor;
	private JTextField textTercerValor;
	private JLabel lblResultSuma;
	private JLabel lblResultMult;
	private JLabel lblResultDiv;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_EjemploObjetos window = new Pantalla_EjemploObjetos();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_EjemploObjetos() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 739, 463);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblOperacionesMatemticas = new JLabel("Operaciones matem\u00E1ticas");
		lblOperacionesMatemticas.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 24));
		lblOperacionesMatemticas.setBounds(247, 11, 294, 32);
		frame.getContentPane().add(lblOperacionesMatemticas);
		
		JLabel lblPrimerValor = new JLabel("primer valor");
		lblPrimerValor.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		lblPrimerValor.setBounds(61, 115, 128, 26);
		frame.getContentPane().add(lblPrimerValor);
		
		JLabel lblSegundoValor = new JLabel("segundo valor");
		lblSegundoValor.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		lblSegundoValor.setBounds(61, 152, 128, 26);
		frame.getContentPane().add(lblSegundoValor);
		
		JLabel lblTercerValor = new JLabel("tercer valor");
		lblTercerValor.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		lblTercerValor.setBounds(61, 188, 128, 26);
		frame.getContentPane().add(lblTercerValor);
		
		textPrimerValor = new JTextField();
		textPrimerValor.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPrimerValor.setBounds(225, 120, 86, 20);
		frame.getContentPane().add(textPrimerValor);
		textPrimerValor.setColumns(10);
		
		textSegundoValor = new JTextField();
		textSegundoValor.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textSegundoValor.setColumns(10);
		textSegundoValor.setBounds(225, 157, 86, 20);
		frame.getContentPane().add(textSegundoValor);
		
		textTercerValor = new JTextField();
		textTercerValor.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textTercerValor.setColumns(10);
		textTercerValor.setBounds(225, 193, 86, 20);
		frame.getContentPane().add(textTercerValor);
		
		JLabel lblLaSumaDe = new JLabel("La suma de los tres valores");
		lblLaSumaDe.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblLaSumaDe.setBounds(339, 75, 294, 32);
		frame.getContentPane().add(lblLaSumaDe);
		
		lblResultSuma = new JLabel("");
		lblResultSuma.setOpaque(true);
		lblResultSuma.setBackground(Color.GREEN);
		lblResultSuma.setFont(new Font("Dialog", Font.ITALIC, 18));
		lblResultSuma.setBounds(379, 120, 229, 38);
		frame.getContentPane().add(lblResultSuma);
		
		JLabel lblLaMultiplicacionDel = new JLabel("La multiplicacion del 1ro y 3ro");
		lblLaMultiplicacionDel.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblLaMultiplicacionDel.setBounds(339, 175, 320, 32);
		frame.getContentPane().add(lblLaMultiplicacionDel);
		
		lblResultMult = new JLabel("");
		lblResultMult.setOpaque(true);
		lblResultMult.setBackground(Color.GREEN);
		lblResultMult.setFont(new Font("Dialog", Font.ITALIC, 18));
		lblResultMult.setBounds(379, 220, 229, 38);
		frame.getContentPane().add(lblResultMult);
		
		JLabel lblLaDivisinDel = new JLabel("La divisi\u00F3n del 2do con el 1ro");
		lblLaDivisinDel.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblLaDivisinDel.setBounds(339, 269, 312, 32);
		frame.getContentPane().add(lblLaDivisinDel);
		
		lblResultDiv = new JLabel("");
		lblResultDiv.setOpaque(true);
		lblResultDiv.setBackground(Color.GREEN);
		lblResultDiv.setFont(new Font("Dialog", Font.ITALIC, 18));
		lblResultDiv.setBounds(379, 314, 229, 38);
		frame.getContentPane().add(lblResultDiv);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//obtencion
				float fPrimerValor=Float.parseFloat(textPrimerValor.getText());
				float fSegundoValor=Float.parseFloat(textSegundoValor.getText());
				float fTercerValor=Float.parseFloat(textTercerValor.getText());
				
				//cuentas
				float fSuma = fPrimerValor + fSegundoValor + fTercerValor;
				float fmult = fPrimerValor * fTercerValor;
				float fdiv  = fSegundoValor / fPrimerValor;
				
				//muestra
				lblResultSuma.setText(fPrimerValor + " + " + fSegundoValor + " + " + fTercerValor + " = " + fSuma);
				lblResultMult.setText(fPrimerValor + " * " + fTercerValor + " = " + fmult);
				lblResultDiv.setText(fSegundoValor + "/" + fPrimerValor + "= " + fdiv);

				
			}
		});

		btnCalcular.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		btnCalcular.setBounds(32, 256, 95, 29);
		frame.getContentPane().add(btnCalcular);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPrimerValor.setText("");
				textSegundoValor.setText("");
				textTercerValor.setText("");
				
				lblResultDiv.setText("");
				lblResultMult.setText("");
				lblResultSuma.setText("");
			}
		});
		btnLimpiar.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		btnLimpiar.setBounds(32, 314, 95, 29);
		frame.getContentPane().add(btnLimpiar);
		
		JButton btnAsignar = new JButton("Asignar");
		btnAsignar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int irandom1 =(int)( Math.random()*10000);
				float frandom1 =(float)irandom1/100;
				textPrimerValor.setText(Float.toString(frandom1));

				irandom1 =(int)( Math.random()*10000);
				frandom1 =(float)irandom1/100;
				textSegundoValor.setText(Float.toString(frandom1));
				
				irandom1 =(int)( Math.random()*10000);
				frandom1 =(float)irandom1/100;
				textTercerValor.setText(Float.toString(frandom1));

			}
		});
		btnAsignar.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		btnAsignar.setBounds(48, 21, 95, 29);
		frame.getContentPane().add(btnAsignar);
	}
}
